mod common;

use crate::common::Git;
use std::fs::{self, OpenOptions};
use std::io::prelude::*;

#[test]
fn simple_config() {
    let root = common::setup_simple_clone();

    let config = fs::read_to_string(root.path().join("forgitten.toml"))
        .expect("cannot read configuration");
    let upstream = common::tmp_path(&root, "upstreams/simple");
    let child = common::tmp_path(&root, "repos/simple");
    let expected = format!("[\"{}\"]\norigin = \"{}\"\n", &child, &upstream);
    assert_eq!(config, expected);
}

#[test]
fn before_and_after_commit() {
    let root = common::setup_simple_clone();
    let repo = root.path().join("repos/simple");
    let options = common::build_test_options(&root);

    // No output with a fresh clone.
    assert_eq!(common::run_forgitten(&options), "");

    // Create a file, which should produce uncommitted changes output.
    fs::write(repo.join("other-file"), b"some random text\n")
        .expect("write to new file failed");
    let wanted = common::build_output(&repo, "master", "uncommitted changes");
    assert_eq!(common::run_forgitten(&options), wanted);

    // Adding the file but not committing it changes nothing.
    let git = Git::new(&repo);
    git.add_all();
    assert_eq!(common::run_forgitten(&options), wanted);

    // Committing it should change the output to a notice that the local
    // branch is ahead of the origin.
    git.commit();
    let wanted = common::build_output(&repo, "master", "ahead 1 of origin");
    assert_eq!(common::run_forgitten(&options), wanted);
}

#[test]
fn fast_forward() {
    let root = common::setup_simple_clone();
    let upstream = root.path().join("upstreams/simple");
    let options = common::build_test_options(&root);

    // Create a file and commit it upstream.
    fs::write(upstream.join("other-file"), b"some random text\n")
        .expect("write to new file failed");
    let git = Git::new(upstream);
    git.commit();

    // The file will not exist in the cloned repo.
    let file = root.path().join("repos/simple/other-file");
    assert!(!file.exists());

    // Running forgitten should produce no output.
    assert_eq!(common::run_forgitten(&options), "");

    // The file should now exist with matching contents.
    let contents = fs::read_to_string(file).expect("file doesn't exist");
    assert_eq!(contents, "some random text\n");
}

#[test]
fn divergence() {
    let root = common::setup_simple_clone();
    let upstream = root.path().join("upstreams/simple");
    let repo = root.path().join("repos/simple");
    let options = common::build_test_options(&root);

    // Create a file and commit it upstream.
    fs::write(upstream.join("upstream-file"), b"some random text\n")
        .expect("write to new file failed");
    let git = Git::new(upstream);
    git.commit();

    // Create a different file and commit it downstream.  Then modify it and
    // commit it again, just to create a number other than one.
    let file = repo.join("downstream-file");
    fs::write(&file, b"some random text\n").expect("write to new file failed");
    let git = Git::new(&repo);
    git.commit();
    {
        let mut fh = OpenOptions::new()
            .append(true)
            .open(file)
            .expect("append to file failed");
        fh.write(b"some more random text\n").expect("append to file failed");
    }
    git.commit();

    // Running forgitten should report the divergence.
    let wanted =
        common::build_output(&repo, "master", "ahead 2 behind 1 of origin");
    assert_eq!(common::run_forgitten(&options), wanted);
}

#[test]
fn feature_branch() {
    let root = common::setup_simple_clone();
    let upstream = root.path().join("upstreams/simple");
    let repo = root.path().join("repos/simple");
    let options = common::build_test_options(&root);

    // Create a second branch also based off of origin/master.  This shouldn't
    // produce any output; it's an empty feature branch.
    let git = Git::new(&repo);
    git.branch_create("feature-branch", "origin/master");
    assert_eq!(common::run_forgitten(&options), "");

    // Create the same branch in the origin and add a commit in the checkout
    // branch.  This should show ahead information, but not complain about the
    // upstream.
    let upstream_git = Git::new(&upstream);
    upstream_git.branch_create("feature-branch", "master");
    git.switch_branch("feature-branch");
    let file = repo.join("downstream-file");
    fs::write(&file, b"some random text\n").expect("write to new file failed");
    git.commit();
    git.switch_branch("master");
    let wanted =
        common::build_output(&repo, "feature-branch", "ahead 1 of origin");
    assert_eq!(common::run_forgitten(&options), wanted);

    // Push the local branch to be even with the origin branch.  This should
    // suppress all output again even though the feature branch is behind its
    // upstream merge branch.
    git.push("origin", "feature-branch");
    assert_eq!(common::run_forgitten(&options), "");
}

#[test]
fn initial_checkout() {
    let root = common::setup_simple_clone();
    let repo = root.path().join("repos/simple");
    let options = common::build_test_options(&root);

    fs::remove_dir_all(&repo).expect("cannot remove checkout");
    assert_eq!(common::run_forgitten(&options), "");
    assert!(repo.join("some-file").exists());
}
