// Utility functions for integration testing.
//
// Doing a proper integration test means not having access to forgitten::git,
// since it's intentionally private.  This therefore duplicates a bit of its
// logic, although in a much simpler form.

// Each test builds its own copy of the common library, so we have to disable
// dead code warnings.
#![allow(dead_code)]

use forgitten::{self, Options};
use std::env;
use std::fs::{self, OpenOptions};
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use std::process::{Command, Output};
use tempfile::{self, TempDir};

pub struct Git {
    path: PathBuf,
}

impl Git {
    pub fn new<T: Into<PathBuf>>(path: T) -> Git {
        Git { path: path.into() }
    }

    pub fn clone(upstream: &Path, path: &Path) -> Git {
        let git = Git::new(path);
        let upstream = upstream.to_string_lossy();
        let path = path.to_string_lossy();
        let args = ["clone", &upstream, &path];
        run_git(&args, None);
        git
    }

    pub fn init(path: &Path) -> Git {
        let git = Git::new(path);
        if !path.is_dir() {
            fs::create_dir(path).expect("cannot create directory");
        }
        git.run(&["init"]);
        git
    }

    pub fn add_all(&self) {
        self.run(&["add", "-A"]);
    }

    pub fn branch_create(&self, name: &str, base: &str) {
        self.run(&["branch", name, base]);
    }

    pub fn commit(&self) {
        self.add_all();
        self.run(&["commit", "-m", "Some testing commit"]);
    }

    pub fn config(&self, name: &str) -> Option<String> {
        let output = self.output(&["config", "--local", name]);
        let output = String::from_utf8_lossy(&output.stdout);
        if output == "" {
            None
        } else {
            Some(String::from(output.trim()))
        }
    }

    pub fn config_replace(&self, name: &str, value: &str) {
        self.run(&["config", "--replace-all", name, value]);
    }

    pub fn fetch(&self) {
        self.run(&["fetch"]);
    }

    pub fn push(&self, remote: &str, branch: &str) {
        self.run(&["push", remote, branch]);
    }

    pub fn switch_branch(&self, name: &str) {
        self.run(&["checkout", name]);
    }

    fn output(&self, args: &[&str]) -> Output {
        Command::new("git")
            .args(args.iter())
            .current_dir(&self.path)
            .output()
            .expect("cannot run git")
    }

    fn run(&self, args: &[&str]) -> Output {
        run_git(args, Some(&self.path))
    }
}

fn run_git(args: &[&str], dir: Option<&Path>) -> Output {
    let mut command = String::from("git ");
    command.push_str(&args.join(" "));
    let error = format!("{} failed", command);

    let mut command = Command::new("git");
    command.args(args.iter());
    if let Some(dir) = dir {
        command.current_dir(dir);
    }
    let output = command.output().expect(&error);
    if !output.status.success() {
        print!("{}", String::from_utf8_lossy(&output.stdout));
        print!("{}", String::from_utf8_lossy(&output.stderr));
        assert!(output.status.success(), error);
    }
    output
}

/// Append an option to the last stanza of the configuration file.  If there's
/// a single repository, this will add the configuration to that repository.
pub fn append_config_option(root: &TempDir, option: &str, value: &str) {
    let line = format!("{} = \"{}\"\n", option, value);
    let mut fh = OpenOptions::new()
        .append(true)
        .open(root.path().join("forgitten.toml"))
        .expect("append to config failed");
    fh.write(line.as_bytes()).expect("append to config failed");
}

/// Build expected forgitten output for a single repository and single branch.
/// Takes the path to the repository, the branch name, and the expected
/// output.
pub fn build_output(repo: &Path, branch: &str, output: &str) -> String {
    format!("{}\n    {}: {}\n", repo.display(), branch, output)
}

/// Returns standard test options for running forgitten given the root
/// temporary directory used for repositories.
pub fn build_test_options(root: &TempDir) -> Options {
    Options {
        all: false,
        config_path: root.path().join("forgitten.toml"),
        log: false,
        path: None,
        verbose: false,
    }
}

/// Creates a temporary directory, sets up two subdirectories `upstreams` and
/// `repos`, creates a repository named `simple` in `upstreams`, and clones it
/// into `repos` with the same name.  This is the base setup for a lot of
/// tests.
///
/// Also disables color from the `colored` crate to make testing easier.
pub fn setup_simple_clone() -> TempDir {
    env::set_var("CLICOLOR", "0");

    let root = tempfile::tempdir().expect("cannot create temporary directory");
    let upstream = root.path().join("upstreams").join("simple");
    let repos = root.path().join("repos");
    fs::create_dir_all(&upstream).expect("cannot create directory");
    fs::create_dir(&repos).expect("cannot create directory");

    fs::write(upstream.join("some-file"), "some test data\n")
        .expect("cannot write test data");
    Git::init(&upstream).commit();

    Git::clone(&upstream, &repos.join("simple"));

    let config =
        forgitten::build_config(&repos).expect("cannot build configuration");
    fs::write(root.path().join("forgitten.toml"), config)
        .expect("cannot save configuration");

    root
}

/// Same as `setup_simple_clone` except add a second branch to both the
/// upstream and the clone.
pub fn setup_two_branch_clone() -> TempDir {
    let root = setup_simple_clone();
    let upstream = root.path().join("upstreams").join("simple");
    let repo = root.path().join("repos").join("simple");

    // Create the second branches.
    Git::new(upstream).branch_create("other", "master");
    let git = Git::new(&repo);
    git.fetch();
    git.branch_create("other", "origin/other");

    root
}

/// Run forgitten and return its output.
pub fn run_forgitten(options: &Options) -> String {
    forgitten::run(&options).expect("forgitten run failed")
}

/// Builds a path relative to the provided temporary directory `root` and
/// returns it as a String.  Assumes there are no Unicode issues converting
/// from OS strings to Rust strings.
pub fn tmp_path(root: &TempDir, path: &str) -> String {
    root.path().join(path).to_string_lossy().into_owned()
}
