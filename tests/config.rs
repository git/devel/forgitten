mod common;

use crate::common::Git;
use forgitten::Error;

#[test]
fn bad_option() {
    let root = common::setup_simple_clone();
    let options = common::build_test_options(&root);

    common::append_config_option(&root, "bogus", "foo");
    let result = forgitten::run(&options);
    match result {
        Err(Error::Config(_)) => (),
        Err(_) => assert!(false, "forgitten returned incorrect error"),
        Ok(_) => assert!(false, "forgitten did not produce an error"),
    }
}

#[test]
fn default_branch() {
    let root = common::setup_two_branch_clone();
    let repo = root.path().join("repos/simple");
    let options = common::build_test_options(&root);

    // Switch to the second branch.
    let git = Git::new(repo);
    git.switch_branch("other");

    // Append the default branch setting to the configuration.
    common::append_config_option(&root, "default_branch", "other");

    // Running forgitten should report no output.
    assert_eq!(common::run_forgitten(&options), "");
}

#[test]
fn user_email() {
    let root = common::setup_simple_clone();
    let repos = root.path().join("repos");
    let repo = repos.join("simple");
    let options = common::build_test_options(&root);

    // Confirm user.email isn't set by default in case Git does something
    // weird in the future.
    let git = Git::new(repo);
    assert_eq!(git.config("user.email"), None);

    // Append the user.email setting and run forgitten.
    common::append_config_option(&root, "user_email", "user@example.com");
    assert_eq!(common::run_forgitten(&options), "");

    // The config option should now be set.
    match git.config("user.email") {
        Some(s) => assert_eq!(s.as_str(), "user@example.com"),
        None => assert!(false, "no user.email set"),
    }

    // Change it to something else and confirm that it's changed back.
    git.config_replace("user.email", "foo@example.org");
    assert_eq!(common::run_forgitten(&options), "");
    match git.config("user.email") {
        Some(s) => assert_eq!(s.as_str(), "user@example.com"),
        None => assert!(false, "no user.email set"),
    }

    // Check that it serializes back correctly.
    let child = common::tmp_path(&root, "repos/simple");
    let upstream = common::tmp_path(&root, "upstreams/simple");
    let expected = format!(
        "[\"{}\"]\nuser_email = \"user@example.com\"\norigin = \"{}\"\n",
        &child, &upstream,
    );
    let config = forgitten::build_config(&repos).expect("cannot build config");
    assert_eq!(config, expected);
}
