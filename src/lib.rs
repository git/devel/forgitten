//! # forgitten
//!
//! forgitten is an application that updates local clones of one or more Git
//! repositories and then reports their status compared to their configured
//! remotes.  Its actions can be controlled the [Options] struct passed into
//! [run] or [run_streaming].
//!
//! ## Configuration
//!
//! Each repository must be configured in forgitten's configuration file, the
//! path to which is specified as part of the [Options] struct.  This
//! configuration file must be in the
//! [TOML](https://github.com/toml-lang/toml) format with one table per
//! repository path, relative to the user's home directory.  Here is a sample
//! configuration entry:
//!
//! ```toml
//! ["dvl/c-tap-harness"]
//! origin = "https://git.eyrie.org/git/devel/c-tap-harness.git"
//! secondary = { github = "git@github.com:rra/c-tap-harness.git" }
//! ```
//!
//! The table key (which should be in quotes since it usually contains `/`) is
//! the path relative to the user's home directory.
//!
//! `origin` sets the URL for the primary upstream repository (see below).
//!
//! `secondary` configures additional remotes (if any) as a table, whose keys
//! are the names of the remotes and whose values are the URLs.  If you have
//! multiple additional remotes, you'll need to use the table syntax, like:
//!
//! ```toml
//! ["dvl/pam-afs-session".secondary]
//! debian = "git@salsa.debian.org:k5-afs/libpam-afs-session.git"
//! github = "git@github.com:rra/pam-afs-session.git"
//! ```
//!
//! `annex` takes a boolean value and says whether this repository is a `git
//! annex` repository instead of a normal Git repository.  This changes how it
//! is updated from upstreams.
//!
//! `ignore` can be set to true to not act on this repository by default.  It
//! will still be acted on if named explicitly on the command line, or if the
//! `all` option is set in `Options`.
//!
//! `user_email` takes a string, and if set, tells forgitten to ensure that
//! the user.email local configuration option is set to the value given.
//!
//! Normally, forgitten will create local branches corresponding to every
//! branch in the `origin` remote.  `sync_branches` can be set to false to
//! suppress this behavior, in which case only already existing branches will
//! be checked and possibly fast-forwarded from the `origin` remote.
//!
//! forgitten will report if the current checked-out branch is other than
//! the value of `default_branch`.  The default if not specified is `master`.
//!
//! ## Actions
//!
//! By default, forgitten takes the following actions on any repository it's
//! told to act on:
//!
//! 1. Check if the repository is not on a branch.  If so, report that and
//!    take no further actions.
//!
//! 2. Check if the current branch has uncommitted changes.  If so, report
//!    that and take no further actions.
//!
//! 3. For each remote mentioned in the configuration file, fetch that remote
//!    to update it, and then prune the remote (to remove dead branches) if it
//!    has any branches whose names do not match local branches.
//!
//! 4. If the repository is a `git annex` repository (set via the `annex`
//!    option in the configuration file), update it with `git annex sync`.
//!
//! 5. If the repository is not a `git annex` repository, create local
//!    branches for each remote branch in the `origin` remote (unless
//!    `sync_branches` was set to false in the configuration file) and set the
//!    remote tracking branch for each local branch to the corresponding
//!    branch in the `origin` remote.  Then, for every local branch that can
//!    be fast-forwarded to a remote branch that it is tracking, do that.
//!
//! 6. Analyze all local branches and report on their relationship with each
//!    remote branch.  Also report if the current local branch is not
//!    `master` (or the `default_branch` configuration option if set).

mod config;
mod error;
mod git;
mod repo;

use crate::config::Config;
use crate::fmt::Display;
use crate::repo::Repository;
use dirs;
use std::fmt;
use std::path::{Path, PathBuf};

pub use crate::error::Error;

/// Configure the actions of [run].  This struct is normally filled in from
/// the command-line options given to the forgitten binary.
#[derive(Debug)]
pub struct Options {
    /// Act on all repositories, even if configured with `ignore = true` in
    /// the configuration file.
    pub all: bool,

    /// Path to the configuration file.
    pub config_path: PathBuf,

    /// After finishing updating and analyzing a repository, print a log of
    /// all Git commands ran that produced any output.  If `verbose` is set,
    /// print a log of all Git commands, whether or not they produced output.
    pub log: bool,

    /// Path to a repository to act on (may be relative).  If `None`, act on
    /// all repositories configured in the configuration file.
    pub path: Option<PathBuf>,

    /// Display all Git commands run even if they didn't produce any output.
    /// Only meaningful in combination with `log`.
    pub verbose: bool,
}

enum OutputMethod {
    Buffer,
    Stream,
}

// Small struct to keep state on console output and control whether that
// output is streamed immediately or accumulated and returned by the output()
// method.  Also adds newlines before each output block, only if there was
// previous output.
struct Output {
    buffer: Option<String>,
    method: OutputMethod,
    need_newline: bool,
}

impl Output {
    fn new(method: OutputMethod) -> Output {
        let buffer = match method {
            OutputMethod::Buffer => Some(String::new()),
            OutputMethod::Stream => None,
        };
        Output { buffer, method, need_newline: false }
    }

    fn add<T: Display>(&mut self, out: T) {
        match self.method {
            OutputMethod::Buffer => self.buffer_append(&format!("{}", out)),
            OutputMethod::Stream => print!("{}", out),
        }
        self.need_newline = true;
    }

    fn add_with_newline<T: Display>(&mut self, out: T) {
        if self.need_newline {
            match self.method {
                OutputMethod::Buffer => self.buffer_append("\n"),
                OutputMethod::Stream => println!(),
            }
        }
        self.add(out);
    }

    fn buffer_append(&mut self, data: &str) {
        self.buffer.as_mut().unwrap().push_str(data);
    }

    fn output(&mut self) -> String {
        match self.method {
            OutputMethod::Buffer => {
                let output = self.buffer.take().unwrap();
                self.buffer = Some(String::new());
                output
            },
            OutputMethod::Stream => String::new(),
        }
    }
}

fn absolute_path(path: &Path) -> Result<PathBuf, Error> {
    if path.is_relative() {
        let mut home = dirs::home_dir().ok_or(Error::HomeDir)?;
        if path.as_os_str() != "." {
            home.push(&path);
        }
        Ok(home)
    } else {
        Ok(path.to_path_buf())
    }
}

/// Generate a forgitten configuration file for all repositories found
/// recursively under `path`.
pub fn build_config(path: &Path) -> Result<String, Error> {
    config::build_config(&path)
}

fn build_repos(options: &Options) -> Result<Vec<Repository>, Error> {
    let config = Config::new(&options.config_path)?;

    let mut repos: Vec<Repository> = Vec::new();
    if options.path.is_some() {
        let path = options.path.as_ref().unwrap();
        let repo_config = config.repos.get(path);
        if repo_config.is_none() {
            return Err(Error::NoConfig(path.to_path_buf()));
        } else {
            let path = absolute_path(path)?;
            repos.push(Repository::new(path, repo_config.unwrap())?);
        }
    } else {
        for (path, repo_config) in &config.repos {
            if !repo_config.ignore || options.all {
                let path = absolute_path(path)?;
                repos.push(Repository::new(path, repo_config)?);
            }
        }
    }

    Ok(repos)
}

fn process_repos(
    options: &Options,
    repos: &mut Vec<Repository>,
    output_method: OutputMethod,
) -> Result<Output, Error> {
    let mut output = Output::new(output_method);

    for repo in repos.iter_mut() {
        repo.process()?;
        if options.log {
            if let Some(log) = repo.log(options.verbose) {
                output.add(log);
            }
        } else if let Some(report) = repo.report() {
            output.add_with_newline(report);
        }
    }

    if options.log {
        for repo in repos.iter() {
            if let Some(report) = repo.report() {
                output.add_with_newline(report);
            }
        }
    }

    Ok(output)
}

/// Run the forgitten application, configured with the `options` argument, and
/// return the resulting report.
pub fn run(options: &Options) -> Result<String, Error> {
    let mut repos = build_repos(&options)?;
    let mut output = process_repos(&options, &mut repos, OutputMethod::Buffer)?;
    Ok(output.output())
}

/// Run the forgitten application, configured with the `options` argument, and
/// print reports or logs to standard output as they are gathered.
pub fn run_streaming(options: &Options) -> Result<(), Error> {
    let mut repos = build_repos(&options)?;
    process_repos(&options, &mut repos, OutputMethod::Stream)?;
    Ok(())
}
