use crate::config::RepositoryConfig;
use crate::git::{Branch, BranchStatus, Git, GitError, Remote, Result};
use colored::Colorize;
use std::collections::{BTreeMap, HashSet};
use std::path::PathBuf;

#[derive(Debug)]
pub struct Repository {
    git: Git,
    path: PathBuf,
    current_branch: Option<String>,
    orig_branch: Option<String>,
    default_branch: String,
    changed_branches: bool,
    annex: bool,
    user_email: Option<String>,
    sync_branches: bool,
    branches: BTreeMap<String, Branch>,
    remotes: BTreeMap<String, Remote>,
    targets: BTreeMap<String, String>,
    report: Vec<String>,
}

impl Repository {
    pub fn new<T>(path: T, config: &RepositoryConfig) -> Result<Repository>
    where
        T: Into<PathBuf>,
    {
        let path = path.into();
        let git = if path.exists() {
            Git::new(&path)
        } else if let Some(ref url) = config.origin {
            Git::clone(url, &path)?
        } else {
            let error = format!(
                "{} does not exist and no origin given",
                path.display(),
            );
            return Err(GitError::new(&[], error, &path));
        };

        let current_branch = git.current_branch()?;
        let orig_branch = current_branch.clone();
        let default_branch = config.default_branch.clone();
        let branches = git.branches()?;
        let remotes = git.remotes()?;

        let mut targets: BTreeMap<String, String> = BTreeMap::new();
        if let Some(ref url) = config.origin {
            targets.insert(String::from("origin"), url.clone());
        }
        if let Some(ref remotes) = config.secondary {
            for (name, url) in remotes {
                targets.insert(name.clone(), url.clone());
            }
        }

        Ok(Repository {
            git,
            path,
            current_branch,
            orig_branch,
            default_branch,
            changed_branches: false,
            annex: config.annex,
            user_email: config.user_email.clone(),
            sync_branches: config.sync_branches,
            branches,
            remotes,
            targets,
            report: Vec::new(),
        })
    }

    pub fn config(&self) -> RepositoryConfig {
        let origin = match self.remotes.get("origin") {
            Some(ref remote) => remote.url.as_ref().cloned(),
            None => None,
        };
        let mut secondary: BTreeMap<String, String> = BTreeMap::new();
        for (name, remote) in &self.remotes {
            if name != "dgit" && name != "origin" && remote.url.is_some() {
                let url = remote.url.as_ref().unwrap().clone();
                secondary.insert(name.clone(), url);
            }
        }
        let secondary =
            if secondary.is_empty() { None } else { Some(secondary) };

        let mut config = RepositoryConfig::empty();
        config.origin = origin;
        config.secondary = secondary;
        config.user_email = self.git.config("user.email");
        config
    }

    pub fn process(&mut self) -> Result<()> {
        if self.current_branch.is_none() {
            let message = "repository not on a branch";
            self.report.push(message.bright_red().to_string());
            Ok(())
        } else if self.git.is_dirty()? {
            let current = self.current_branch.as_ref().unwrap();
            let status = "uncommitted changes".bright_yellow();
            self.report.push(format!("{}: {}", current, status));
            Ok(())
        } else {
            self.fix_config()?;
            self.update_remotes()?;
            if self.annex {
                self.git.annex_sync()?;
                self.branches = self.git.branches()?;
            } else {
                self.fix_upstreams()?;
                self.update_branches()?;
            }
            self.analyze_branches()
        }
    }

    pub fn log(&self, verbose: bool) -> Option<String> {
        self.git.log(verbose)
    }

    pub fn report(&self) -> Option<String> {
        if self.report.is_empty() {
            None
        } else {
            let mut report = format!("{}", self.path.display());
            report.push('\n');
            for line in &self.report {
                report.push_str("    ");
                report.push_str(&line);
                report.push('\n');
            }
            Some(report)
        }
    }

    /// Determine if this branch's upstream is an interesting target against
    /// which to report status, and if so, return that status.  The default is
    /// to assume this is interesting, but make an exception if the upstream
    /// is origin/master and the branch appears in one of the remotes.  Status
    /// against the remote will be reported elsewhere.
    fn analyze_branch_status(
        &self,
        name: &str,
        branch: &Branch,
    ) -> Option<String> {
        let upstream = branch.upstream.as_ref().unwrap();
        if upstream == "origin/master" && name != "master" {
            for remote in self.targets.keys() {
                if let Some(remote) = self.remotes.get::<str>(remote) {
                    if remote.branches.iter().any(|b| b == name) {
                        return None;
                    }
                }
            }
        }
        branch
            .status
            .as_ref()
            .and_then(|ref s| self.format_branch_status(s, upstream))
    }

    fn analyze_branch_upstream(
        &self,
        name: &str,
        branch: &Branch,
    ) -> Option<String> {
        if let Some(ref upstream) = branch.upstream {
            let origin = format!("origin/{}", name);
            if upstream != &origin && upstream != "origin/master" {
                return Some(format!("upstream {}", upstream));
            }
        }
        None
    }

    fn analyze_branches(&mut self) -> Result<()> {
        for (name, branch) in &self.branches {
            let mut output = Vec::new();

            if let Some(m) = self.analyze_branch_upstream(name, branch) {
                output.push(m);
            }

            match branch.upstream {
                None if name == "git-annex" => (),
                None => output.push("no upstream branch".yellow().to_string()),
                Some(_) => {
                    if let Some(m) = self.analyze_branch_status(name, branch) {
                        output.push(m);
                    }
                },
            }

            for remote in self.targets.keys() {
                if let Some(remote) = self.remotes.get::<str>(remote) {
                    if !remote.branches.contains(name) {
                        continue;
                    }
                } else {
                    continue;
                }

                let upstream = &format!("{}/{}", remote, name);
                if Some(upstream) != branch.upstream.as_ref() {
                    if let Some(status) = self.branch_status(name, upstream)? {
                        output.push(status);
                    }
                }
            }

            if let Some(ref current_branch) = self.orig_branch {
                if name == current_branch && name != &self.default_branch {
                    output.push("current branch".green().to_string());
                }
            }

            if !output.is_empty() {
                self.report.push(format!("{}: {}", name, output.join(", ")));
            }
        }
        Ok(())
    }

    fn branch_status(
        &self,
        branch: &str,
        upstream: &str,
    ) -> Result<Option<String>> {
        let status = self.git.branch_status(branch, upstream)?;
        Ok(self.format_branch_status(&status, upstream))
    }

    fn fix_config(&mut self) -> Result<()> {
        if let Some(ref email) = self.user_email {
            let current_email = self.git.config("user.email");
            if current_email.as_ref() != Some(email) {
                self.git.config_replace("user.email", email)?;
            }
        }
        Ok(())
    }

    fn fix_upstreams(&mut self) -> Result<()> {
        if !self.remotes.contains_key("origin") {
            return Ok(());
        }

        for name in self.remotes["origin"].branches.iter().cloned() {
            let upstream = format!("origin/{}", &name);
            if !self.branches.contains_key(&name) {
                if self.sync_branches {
                    self.branches.insert(
                        name.clone(),
                        self.git.create_tracking_branch(&name, &upstream)?,
                    );
                }
            } else if self.branches[&name].upstream.is_none() {
                self.git.set_upstream(&name, &upstream)?;
                let branch = self.git.branch(&name)?;
                self.branches.insert(name, branch);
            }
        }
        Ok(())
    }

    fn format_branch_status(
        &self,
        status: &BranchStatus,
        upstream: &str,
    ) -> Option<String> {
        if status.is_up_to_date() {
            None
        } else {
            let remote = upstream.split('/').next().unwrap_or(upstream);
            let status = status.to_colored_string();
            Some(format!("{} of {}", status, remote))
        }
    }

    fn switch_branch(&mut self, branch: &str) -> Result<()> {
        self.git.switch_branch(branch)?;
        self.current_branch = Some(String::from(branch));
        if let Some(ref orig_branch) = self.orig_branch {
            self.changed_branches = branch != orig_branch;
        } else {
            self.changed_branches = true;
        }
        Ok(())
    }

    fn update_branches(&mut self) -> Result<()> {
        let branches: Vec<(String, String)> = self
            .branches
            .iter()
            .filter(|&(_, ref v)| {
                v.status
                    .as_ref()
                    .map(|s| s.is_fast_forwardable())
                    .unwrap_or(false)
            })
            .map(|(k, v)| (k.clone(), v.upstream.as_ref().unwrap().clone()))
            .collect();

        for (name, upstream) in branches {
            self.switch_branch(&name)?;
            self.git.fast_forward(&upstream)?;
            let branch = self.git.branch(&name)?;
            self.branches.insert(name, branch);
        }

        Ok(())
    }

    fn update_remotes(&mut self) -> Result<()> {
        let branches: HashSet<String> = self.branches.keys().cloned().collect();

        for (name, url) in &self.targets {
            if !self.remotes.contains_key(name) {
                let remote = self.git.remote_add(name, url)?;
                self.remotes.insert(name.clone(), remote);
            }
            let mut remote = self.remotes.get_mut(name).unwrap();
            if Some(url) != remote.url.as_ref() {
                self.git.remote_set_url(name, &url)?;
                remote.url = Some(url.clone());
            }
            self.git.fetch(&name)?;
            let rbranches: HashSet<String> =
                remote.branches.iter().cloned().collect();
            if rbranches.difference(&branches).next().is_some() {
                self.git.remote_prune(&name)?;
                *remote = self.git.remote(&name)?;
            }
        }
        self.branches = self.git.branches()?;
        Ok(())
    }
}

impl Drop for Repository {
    fn drop(&mut self) {
        if !self.changed_branches {
            return;
        }
        if let Some(ref branch) = self.orig_branch.take() {
            if let Err(e) = self.git.switch_branch(&branch) {
                let error = format!(
                    "Cannot restore original branch for {}",
                    self.path.display(),
                );
                eprintln!("{}", error.bright_red());
                e.report();
            }
        }
    }
}
