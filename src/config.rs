use crate::error::Error;
use crate::repo::Repository;
use dirs;
use serde_derive::Deserialize;
use std::collections::BTreeMap;
use std::fs::File;
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use toml;
use walkdir::WalkDir;

#[derive(Debug)]
pub struct Config {
    pub repos: BTreeMap<PathBuf, RepositoryConfig>,
}

#[serde(deny_unknown_fields)]
#[derive(Clone, Debug, Deserialize)]
pub struct RepositoryConfig {
    pub origin: Option<String>,
    pub secondary: Option<BTreeMap<String, String>>,
    pub user_email: Option<String>,

    #[serde(default)]
    pub annex: bool,

    #[serde(default = "default_branch")]
    pub default_branch: String,

    #[serde(default)]
    pub ignore: bool,

    #[serde(default = "default_true")]
    pub sync_branches: bool,
}

fn default_branch() -> String {
    String::from("master")
}

fn default_true() -> bool {
    true
}

impl Config {
    pub fn new(path: &Path) -> Result<Config, Error> {
        match File::open(path) {
            Err(e) => Err(Error::from_io_error("Cannot open", path, e)),
            Ok(file) => Config::from_file(&file, path),
        }
    }

    fn from_file(mut file: &File, path: &Path) -> Result<Config, Error> {
        let mut contents = String::new();
        match file.read_to_string(&mut contents) {
            Err(e) => Err(Error::from_io_error("Cannot read", path, e)),
            Ok(_) => Ok(Config { repos: toml::from_str(&contents)? }),
        }
    }
}

impl RepositoryConfig {
    pub fn empty() -> RepositoryConfig {
        RepositoryConfig {
            origin: None,
            secondary: None,
            user_email: None,
            annex: false,
            default_branch: String::from("master"),
            ignore: false,
            sync_branches: false,
        }
    }
}

fn canonicalize_path(path: &Path) -> Result<PathBuf, Error> {
    let path = path
        .canonicalize()
        .map_err(|e| Error::from_io_error("Bad path", path, e))?;
    Ok(match dirs::home_dir() {
        Some(ref home) if &path == home => PathBuf::from("."),
        Some(home) => path.strip_prefix(&home).unwrap_or(&path).to_path_buf(),
        None => path.to_path_buf(),
    })
}

pub fn build_config(path: &Path) -> Result<String, Error> {
    let entries = WalkDir::new(path)
        .sort_by(|a, b| a.file_name().cmp(b.file_name()))
        .into_iter()
        .filter_entry(|e| e.file_type().is_dir() && e.file_name() != ".git");
    let mut repos = Vec::new();

    for entry in entries {
        if let Err(e) = entry {
            return Err(e.into());
        }
        let path = entry.unwrap().path().to_path_buf();
        if path.join(".git").exists() {
            repos.push(path);
        }
    }

    let mut output = String::new();
    for path in repos {
        let repo = Repository::new(&path, &RepositoryConfig::empty())?;
        let key = canonicalize_path(&path)?;
        let config = repo.config();
        if !output.is_empty() {
            output.push('\n');
        }
        output.push_str(&format!("[\"{}\"]\n", key.display()));
        if config.user_email.is_some() {
            let user_email = config.user_email.unwrap();
            output.push_str(&format!("user_email = \"{}\"\n", user_email));
        }
        if config.origin.is_some() {
            let origin = config.origin.unwrap();
            output.push_str(&format!("origin = \"{}\"\n", origin));
        }
        if config.secondary.is_some() {
            let secondary = config.secondary.unwrap();
            if secondary.len() == 1 {
                let (name, url) = secondary.iter().next().unwrap();
                output.push_str(&format!(
                    "secondary = {{ {} = \"{}\" }}\n",
                    name, url
                ));
            } else {
                let heading = &format!("\n[\"{}\".secondary]\n", key.display());
                output.push_str(heading);
                for (name, url) in secondary {
                    output.push_str(&format!("{} = \"{}\"\n", name, url));
                }
            }
        }
    }

    Ok(output)
}
