use forgitten::{Error, Options};
use std::path::PathBuf;
use std::process;
use structopt::StructOpt;
use xdg;

#[derive(Debug, StructOpt)]
struct CommandOptions {
    /// Act on all repositories, even ignored ones
    #[structopt(short, long)]
    all: bool,

    /// Build a configuration file from Git checkouts
    #[structopt(short, long)]
    build_config: bool,

    /// Path to configuration file
    #[structopt(short, long, parse(from_os_str))]
    config: Option<PathBuf>,

    /// Display a log of every update command run
    #[structopt(short, long)]
    log: bool,

    /// Log even commands that produce no output
    #[structopt(short, long)]
    verbose: bool,

    /// Git repository or directory tree on which to operate
    #[structopt(parse(from_os_str))]
    path: Option<PathBuf>,
}

fn exit_with_error(error: &Error) -> ! {
    eprintln!("{}", error);
    process::exit(1);
}

fn find_config(path: &Option<PathBuf>) -> PathBuf {
    let mut config_path = None;

    if let Some(ref path) = path {
        config_path = Some(PathBuf::from(path))
    } else if let Ok(xdg_dirs) = xdg::BaseDirectories::new() {
        config_path = xdg_dirs.find_config_file("forgitten.toml");
    }

    match config_path {
        None => exit_with_error(&Error::NoConfigFile),
        Some(path) => path,
    }
}

fn build_config(path: Option<PathBuf>) -> ! {
    let path = path.unwrap_or_else(|| PathBuf::from("."));
    match forgitten::build_config(&path) {
        Err(error) => exit_with_error(&error),
        Ok(output) => print!("{}", output),
    }
    process::exit(0);
}

fn run(options: CommandOptions) {
    let config = Options {
        all: options.all,
        config_path: find_config(&options.config),
        log: options.log,
        path: options.path,
        verbose: options.verbose,
    };

    if let Err(error) = forgitten::run_streaming(&config) {
        exit_with_error(&error);
    }
}

fn main() {
    let options = CommandOptions::from_args();

    if options.build_config {
        build_config(options.path);
    } else {
        run(options);
    }
}
