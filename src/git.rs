use colored::{ColoredString, Colorize};
use lazy_static::lazy_static;
use std::collections::{BTreeMap, HashSet};
use std::error::Error;
use std::fmt;
use std::io;
use std::path::{Path, PathBuf};
use std::process::{Command, Output};
use std::result;

pub type Result<T> = result::Result<T, GitError>;

lazy_static! {
    static ref IGNORE_BRANCHES: HashSet<&'static str> = {
        let mut s = HashSet::new();
        s.insert("HEAD");
        s.insert("git-annex");
        s.insert("synced/git-annex");
        s.insert("synced/master");
        s
    };
}

#[derive(Debug, Clone)]
pub struct Branch {
    pub status: Option<BranchStatus>,
    pub upstream: Option<String>,
}

#[derive(Debug, Clone)]
pub struct BranchStatus {
    ahead: u32,
    behind: u32,
}

#[derive(Debug, Clone)]
pub struct Remote {
    pub url: Option<String>,
    pub branches: Vec<String>,
}

#[derive(Debug)]
pub struct GitError {
    args: String,
    path: PathBuf,
    summary: String,
    output: Option<Output>,
    cause: Option<io::Error>,
}

#[derive(Debug)]
struct LogEntry {
    command: String,
    output: String,
}

#[derive(Debug)]
pub struct Git {
    path: PathBuf,
    log: Vec<LogEntry>,
}

impl Branch {
    fn new(name: &str, upstream: Option<String>, git: &Git) -> Result<Branch> {
        match upstream {
            Some(upstream) => Ok(Branch {
                status: Some(git.branch_status(name, &upstream)?),
                upstream: Some(upstream),
            }),
            None => Ok(Branch { status: None, upstream: None }),
        }
    }
}

impl BranchStatus {
    pub fn is_fast_forwardable(&self) -> bool {
        self.ahead == 0 && self.behind > 0
    }

    pub fn is_up_to_date(&self) -> bool {
        self.ahead == 0 && self.behind == 0
    }

    pub fn to_colored_string(&self) -> ColoredString {
        let base = self.to_string();
        match (self.ahead, self.behind) {
            (0, 0) => base.white(),
            (0, _) => base.green(),
            (_, 0) => base.yellow(),
            (_, _) => base.bright_red(),
        }
    }
}

impl fmt::Display for BranchStatus {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match (self.ahead, self.behind) {
            (0, 0) => write!(f, "up to date"),
            (0, b) => write!(f, "behind {}", b),
            (a, 0) => write!(f, "ahead {}", a),
            (a, b) => write!(f, "ahead {} behind {}", a, b),
        }
    }
}

impl GitError {
    pub fn new(args: &[&str], summary: String, path: &Path) -> GitError {
        GitError {
            args: args.join(" "),
            path: path.into(),
            summary,
            output: None,
            cause: None,
        }
    }

    fn from_io_error(args: &[&str], err: io::Error, path: &Path) -> GitError {
        GitError {
            args: args.join(" "),
            path: path.to_path_buf(),
            summary: String::from(err.description()),
            output: None,
            cause: Some(err),
        }
    }

    fn from_output(args: &[&str], output: Output, path: &Path) -> GitError {
        GitError {
            args: args.join(" "),
            path: path.to_path_buf(),
            summary: format!("{}", output.status),
            output: Some(output),
            cause: None,
        }
    }

    pub fn output(&self) -> Option<String> {
        self.output
            .as_ref()
            .map(|o| String::from_utf8_lossy(&o.stderr).into_owned())
    }

    pub fn report(&self) {
        let error = format!("git {} failed: {}", &self.args, &self.summary);
        eprintln!("{}", error.bright_red());
        eprintln!("Working directory: {}", self.path.display());
        if let Some(ref output) = self.output {
            eprint!("{}", String::from_utf8_lossy(&output.stderr));
        }
    }
}

impl fmt::Display for GitError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "git {} failed: {} (in {})",
            &self.args,
            &self.summary,
            self.path.display(),
        )
    }
}

impl Error for GitError {
    // clippy wants self.cause.as_ref(), but rustc rejects that with a type
    // incompatibility error because it apparently cannot recognize that
    // Option<&io::Error> is compatible with Option<&dyn Error>.
    #[allow(clippy::match_as_ref)]
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self.cause {
            Some(ref cause) => Some(cause),
            None => None,
        }
    }
}

impl Git {
    pub fn new<T: Into<PathBuf>>(path: T) -> Git {
        Git { path: path.into(), log: Vec::new() }
    }

    pub fn clone<T: Into<PathBuf>>(url: &str, path: T) -> Result<Git> {
        let path = path.into();
        let curdir = PathBuf::from(".");
        let mut git = Git::new(curdir);
        git.git_run(&["clone", url, &path.to_string_lossy()])?;
        git.path = path;
        Ok(git)
    }

    pub fn annex_sync(&mut self) -> Result<()> {
        self.git_run(&["annex", "sync"])
    }

    pub fn branch(&self, name: &str) -> Result<Branch> {
        let rname = format!("refs/heads/{}", name);
        let args = ["for-each-ref", "--format=%(upstream:short)", &rname];
        Branch::new(name, self.list_one(&args)?, self)
    }

    pub fn branch_status(
        &self,
        branch: &str,
        upstream: &str,
    ) -> Result<BranchStatus> {
        let refs = format!("{}...{}", branch, upstream);
        let args = ["rev-list", "--left-right", "--count", &refs];
        let output = self.git(&args)?;
        let counts: Vec<u32> = String::from_utf8_lossy(&output.stdout)
            .split_whitespace()
            .map(|n| n.parse::<u32>().unwrap())
            .collect();

        if counts.len() != 2 {
            let summary = format!("expected 2 items, found {}", counts.len());
            Err(GitError::new(&args, summary, &self.path))
        } else {
            Ok(BranchStatus { ahead: counts[0], behind: counts[1] })
        }
    }

    pub fn branches(&self) -> Result<BTreeMap<String, Branch>> {
        let mut branches = BTreeMap::new();
        let args = [
            "for-each-ref",
            "--format=%(refname:lstrip=2) %(upstream:short)",
            "refs/heads/**",
        ];
        for line in self.list(&args)? {
            let refs: Vec<&str> = line.split_whitespace().collect();

            if refs.is_empty() || refs.len() > 2 {
                let e = format!("expected 1 or 2 items, found {}", refs.len());
                return Err(GitError::new(&args, e, &self.path));
            }

            let name = String::from(refs[0]);
            if IGNORE_BRANCHES.contains::<str>(&name) {
                continue;
            }
            let upstream = refs.get(1).map(|u| String::from(*u));
            let branch = Branch::new(&name, upstream, self)?;
            branches.insert(name, branch);
        }
        Ok(branches)
    }

    pub fn config(&self, name: &str) -> Option<String> {
        self.list_one(&["config", "--local", name]).unwrap_or(None)
    }

    pub fn config_replace(&mut self, name: &str, value: &str) -> Result<()> {
        self.git_run(&["config", "--replace-all", name, value])
    }

    pub fn create_tracking_branch(
        &mut self,
        name: &str,
        upstream: &str,
    ) -> Result<Branch> {
        self.git_run(&["branch", name, upstream])?;
        self.branch(name)
    }

    pub fn current_branch(&self) -> Result<Option<String>> {
        match self.list_one(&["rev-parse", "--abbrev-ref", "HEAD"]) {
            Ok(Some(ref s)) if s == "HEAD" => Ok(None),
            Ok(x) => Ok(x),
            Err(_) => Ok(None),
        }
    }

    pub fn fast_forward(&mut self, to: &str) -> Result<()> {
        self.git_run(&["merge", "--ff-only", to])
    }

    pub fn fetch(&mut self, remote: &str) -> Result<()> {
        self.git_run(&["fetch", remote])
    }

    pub fn is_dirty(&self) -> Result<bool> {
        self.list(&["status", "--porcelain"]).map(|o| !o.is_empty())
    }

    pub fn log(&self, verbose: bool) -> Option<String> {
        let mut log = String::new();
        for entry in &self.log {
            if verbose || !entry.output.is_empty() {
                let command = format!("$ {}", &entry.command);
                log.push_str(&command.bright_black());
                log.push('\n');
                log.push_str(&entry.output);
            }
        }

        if log.is_empty() {
            None
        } else {
            let dir = format!("# In directory {}", &self.path.display());
            Some(format!("{}\n{}", dir.bright_black(), log))
        }
    }

    pub fn remote_add(&mut self, remote: &str, url: &str) -> Result<Remote> {
        self.git_run(&["remote", "add", remote, url])?;
        self.remote(remote)
    }

    pub fn remote_prune(&mut self, remote: &str) -> Result<()> {
        self.git_run(&["remote", "prune", remote])
    }

    pub fn remote_set_url(&mut self, remote: &str, url: &str) -> Result<()> {
        self.git_run(&["remote", "set-url", remote, url])
    }

    pub fn remote(&self, name: &str) -> Result<Remote> {
        let url = self.list_one(&["remote", "get-url", name])?;
        let pattern = format!("refs/remotes/{}/**", name);
        let args = &["for-each-ref", "--format=%(refname:lstrip=3)", &pattern];
        let mut branches = self.list(args)?;
        branches.retain(|r| !IGNORE_BRANCHES.contains::<str>(&r));
        Ok(Remote { url, branches })
    }

    pub fn remotes(&self) -> Result<BTreeMap<String, Remote>> {
        let mut remotes = BTreeMap::new();
        for name in self.list(&["remote"])? {
            let remote = self.remote(&name)?;
            remotes.insert(name, remote);
        }
        Ok(remotes)
    }

    pub fn set_upstream(&mut self, branch: &str, upstream: &str) -> Result<()> {
        let upstream_flag = format!("--set-upstream-to={}", upstream);
        self.git_run(&["branch", &upstream_flag, branch])
    }

    pub fn switch_branch(&mut self, branch: &str) -> Result<()> {
        self.git_run(&["checkout", branch])
    }

    fn git(&self, args: &[&str]) -> Result<Output> {
        let output = Command::new("git")
            .args(args.iter())
            .current_dir(&self.path)
            .output();

        match output {
            Err(err) => Err(GitError::from_io_error(args, err, &self.path)),
            Ok(output) => {
                if !output.status.success() {
                    Err(GitError::from_output(args, output, &self.path))
                } else {
                    Ok(output)
                }
            },
        }
    }

    fn list(&self, args: &[&str]) -> Result<Vec<String>> {
        let output = self.git(args)?;
        let lines: Vec<String> = String::from_utf8_lossy(&output.stdout)
            .lines()
            .map(|s| String::from(s.trim()))
            .collect();
        Ok(lines)
    }

    fn list_one(&self, args: &[&str]) -> Result<Option<String>> {
        let output = self.list(args)?;
        if output.len() > 1 {
            let summary = format!("expected 1 line, got {}", output.len());
            return Err(GitError::new(args, summary, &self.path));
        }
        match output.into_iter().next() {
            Some(ref o) if o.is_empty() => Ok(None),
            o => Ok(o),
        }
    }

    fn git_run(&mut self, args: &[&str]) -> Result<()> {
        let mut command = String::from("git ");
        command.push_str(&args.join(" "));

        let raw = self.git(args)?;
        let mut output = String::from_utf8_lossy(&raw.stdout).into_owned();
        output.push_str(&String::from_utf8_lossy(&raw.stderr));
        self.log.push(LogEntry { command, output });

        Ok(())
    }
}
