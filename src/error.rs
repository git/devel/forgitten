use crate::fmt;
use crate::git::GitError;
use colored::Colorize;
use std::error;
use std::io;
use std::path::{Path, PathBuf};
use toml;
use walkdir;

/// Error class intended for display on the console.
///
/// `Error` supports conversions from various other error types and
/// constructors that create an error given an error message or a few other
/// types of failures.
///
/// When formatted for display, an `Error` will produce an error message in
/// bold red, possibly followed by some supplemental information.
#[derive(Debug)]
pub enum Error {
    /// Error parsing the configuration file.
    Config(toml::de::Error),

    /// Error running some Git command.
    Git(GitError),

    /// Unable to determine the user's home directory when expanding relative
    /// paths in the configuration file.
    HomeDir,

    /// I/O error with file read or write or with path handling.
    Io {
        /// Message explaining the overall error.
        message: String,
        /// Path for which the error occurred.
        path: PathBuf,
        /// Underlying `std::io::Error` for this error.
        error: io::Error,
    },

    /// No configuration found for a given repository.
    NoConfig(PathBuf),

    /// No forgitten configuration file found.
    NoConfigFile,

    /// Error walking the directory structure to find repositories when
    /// building the configuration.
    Walk(walkdir::Error),
}

impl Error {
    /// Create an [Error::Io] from an [io::Error], including a path and a
    /// prefix error message alongside the underlying error.
    pub fn from_io_error<T>(message: T, path: &Path, error: io::Error) -> Error
    where
        T: Into<String>,
    {
        Error::Io { message: message.into(), path: path.to_path_buf(), error }
    }
}

impl From<GitError> for Error {
    fn from(error: GitError) -> Self {
        Error::Git(error)
    }
}

impl From<toml::de::Error> for Error {
    fn from(error: toml::de::Error) -> Self {
        Error::Config(error)
    }
}

impl From<walkdir::Error> for Error {
    fn from(error: walkdir::Error) -> Self {
        Error::Walk(error)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let err = match self {
            Error::Config(ref e) => {
                let mut err = String::from("Cannot parse config");
                if let Some((line, col)) = e.line_col() {
                    err.push_str(&format!(" at line {}, column {}", line, col));
                };
                err.push_str(&format!(": {}", e));
                err
            },
            Error::Git(ref e) => e.to_string(),
            Error::HomeDir => String::from("Cannot determine home directory"),
            Error::Io { message, path, error } => {
                format!("{} {}: {}", message, path.display(), error)
            },
            Error::NoConfig(ref p) => {
                format!("No configuration found for {}", p.display())
            },
            Error::NoConfigFile => String::from("No configuration file found"),
            Error::Walk(ref e) => e.to_string(),
        };
        writeln!(f, "{}", err.bright_red())?;

        if let Error::Git(ref e) = *self {
            if let Some(output) = e.output() {
                write!(f, "{}", output)?;
            }
        }

        Ok(())
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            Error::Config(ref e) => Some(e),
            Error::Git(ref e) => Some(e),
            Error::Io { ref error, .. } => Some(error),
            Error::Walk(ref e) => Some(e),
            _ => None,
        }
    }
}
